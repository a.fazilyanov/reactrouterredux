import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import LoginPage from '../components/LoginPage';
import actionLogin from '../actions/actionLogin';

const mapStateToProps = (state) => {
    return {
        userInfo: state.userInfo
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userInfoReducer: bindActionCreators(actionLogin, dispatch)
    }
};

export const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(LoginPage);