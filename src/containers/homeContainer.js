import { connect } from 'react-redux';
import HomePage from '../components/HomePage';

const mapHomeStateToProps = (state) => {
    
    return {
        userInfo: state.userInfo
    }
};

export const HomeContainer = connect(mapHomeStateToProps,null)(HomePage);