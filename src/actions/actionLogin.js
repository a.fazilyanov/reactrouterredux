export const LOGIN_ACTION = "LOGIN_ACTION"

function actionLogin(value) {
        return {
                type: LOGIN_ACTION,
                userInfo: value
        };
}

export default actionLogin;