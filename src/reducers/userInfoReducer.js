import {LOGIN_ACTION} from '../actions/actionLogin';

const initialState = { 
    userInfo: { 
        authorized: false, 
        userName: "-" 
    }
};

export default function userInfoReducer(state = initialState, action) {
    
    switch (action.type) {
        case LOGIN_ACTION:
            return { ...state, userInfo: action.userInfo};
        default:
            return state;
    }
}