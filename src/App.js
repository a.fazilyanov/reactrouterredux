import React from 'react';
import {Link, BrowserRouter, Switch, Route } from 'react-router-dom';
import { Nav } from 'react-bootstrap';
import {HomeContainer}  from './containers/homeContainer';
import {LoginContainer} from './containers/loginContainer';
import RegisterPage from './components/RegisterPage';
import NotFoundPage from './components/NotFoundPage';

function App() {
  return (
    <BrowserRouter>

      <Nav>
        <Nav.Item className="nav navbar-nav">
          <Nav.Link as={Link} to="/">Home</Nav.Link>
        </Nav.Item>
        
        <Nav.Item className="nav navbar-nav">
          <Nav.Link as={Link} to="/register">Register</Nav.Link>
        </Nav.Item>
        
        <Nav.Item className="nav navbar-nav">
          <Nav.Link as={Link} to="/login">Login</Nav.Link>
        </Nav.Item>
      </Nav>        

      <Switch>
        <Route path="/" exact><HomeContainer /></Route>
        <Route path="/register"><RegisterPage /></Route>
        <Route path="/login"><LoginContainer /></Route>
        <Route path="*"><NotFoundPage /></Route>
      </Switch>

    </BrowserRouter>
      
  );
}

export default App;
