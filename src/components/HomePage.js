import React from 'react';

class HomePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            authorized: this.props.userInfo.authorized,
            userName: this.props.userInfo.userName
        };
    }

    render() {

        const authorized = this.state.authorized;
        const userName = this.state.userName;
        return (
            <h3>
                {authorized ? <>authorized. userName:{userName}</> : <> please sign in </>}
            </h3>
        );
    }
}

export default HomePage;
