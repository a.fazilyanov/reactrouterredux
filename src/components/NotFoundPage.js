import React from 'react';

const NotFoundPage = () => {
    return(
        <h4>
            404, page not found
        </h4> 
    )
}    

export default NotFoundPage;
