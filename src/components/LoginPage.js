import React from 'react';
import LOGIN_ACTION from '../actions/actionLogin';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            authorized: this.props.userInfo.authorized,
            userName: this.props.userInfo.userName,
        };
    }

    handleUserNameChanged = (event) => {
        this.setState({ userName: event.target.value });
    }

    onSubmit = (e) => {
        
        e.preventDefault();

        let userInfo = {            
            userName: this.state.userName,
            authorized: true
        }        

        this.props.userInfoReducer(userInfo, LOGIN_ACTION);

    };

    render() {

        return (
            <form onSubmit={this.onSubmit}>
                <label>Login: <input type="text" name="username" value={this.state.userName} onChange={this.handleUserNameChanged}/></label><br />
                <label>Password: <input type="password" name="userpassword" /></label><br />
                <input type="submit" value="Send" />
            </form>
        );
    }
}

export default LoginPage;